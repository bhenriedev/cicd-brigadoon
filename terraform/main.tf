terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }

  backend "http" {}
}

# Configure the AWS Provider
provider "aws" {
  region = "us-west-1"
  default_tags {
    tags = {
      created_by = "terraform"
    }
  }
}

resource "random_uuid" "test" {}

module "default_vpc" {
  source = "./modules/aws-vpc-default"
}

module "website_s3_bucket" {
  source      = "./modules/aws-s3-bucket-public"
  bucket_name = "s3-${random_uuid.test.result}"

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

# Requires A VPC ID to be passed in
module "securitygroup_web" {
  source         = "./modules/aws-securitygroup-web"
  default_vpc_id = module.default_vpc.name
}

module "aws-iam-web" {
  source           = "./modules/aws-iam-web"
  s3-bucket-id     = module.website_s3_bucket.name
  repo_credentials_arn = aws_secretsmanager_secret.registry.arn
  ec2-profile-name = "iam-${random_uuid.test.result}"
}

module "ec2-docker-server" {
  source               = "./modules/aws-ec2-docker"
  iam_instance_profile = module.aws-iam-web.iam-name
  key_name             = var.aws_key_name
  security_groups      = var.security_groups
  ec2_instance_type    = var.ec2_instance_type
  instance_name        = "ec2-${random_uuid.test.result}"
  CI_REGISTRY          = var.CI_REGISTRY
  CI_REGISTRY_PASSWORD = var.TF_PASSWORD
  CI_REGISTRY_USER     = var.TF_USERNAME
  image_name           = var.IMAGE_NAME

  depends_on = [
    module.securitygroup_web,
  ]
}

module "s3_file_upload" {
  source         = "./modules/aws-s3-file"
  s3_bucket_id   = module.website_s3_bucket.name
  build_filename = var.frontend_file_name
  build_filepath = var.frontend_file_path
}

# module "fargate" {
#   depends_on = [aws_secretsmanager_secret.registry]
#   source               = "./modules/fargate"
#   cluster_name         = "cicd-brigadoon-api"
#   network_mode         = "awsvpc"
#   repo_credentials_arn = aws_secretsmanager_secret.registry.arn
#   iam_role_id = module.aws-iam-web.iam-id
#   vpc_id = module.default_vpc.name
#   container_image = var.IMAGE_NAME
#   execution_role_arn = module.aws-iam-web.iam-arn
#   launch_type = {
#     type   = "FARGATE"
#     cpu    = 256
#     memory = 512
#   }
# }