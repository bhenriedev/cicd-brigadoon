resource "aws_secretsmanager_secret" "registry" {
  name = "gitlab_registry"
}
resource "aws_secretsmanager_secret_version" "sversion" {
  secret_id     = aws_secretsmanager_secret.registry.id
  secret_string = <<EOF
{
"username": "${var.TF_USERNAME}",
"password": "${var.TF_PASSWORD}"
}
EOF
}
data "aws_secretsmanager_secret" "registry" {
  arn = aws_secretsmanager_secret.registry.arn
}

data "aws_secretsmanager_secret_version" "creds" {
  secret_id = data.aws_secretsmanager_secret.registry.arn
}
