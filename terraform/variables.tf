variable "aws_key_name" {
  type        = string
  description = "Name of AWS key for accessing EC2 instance"
}

variable "security_groups" {
  description = "security groups to add"
  type        = list(string)
}

variable "ec2_instance_type" {
  description = "size of EC2 instance to create"
  type        = string
}

variable "CI_REGISTRY" { type = string }
variable "TF_USERNAME" { type = string }
variable "TF_PASSWORD" { type = string }
variable "IMAGE_NAME" { type = string }
variable "frontend_file_path" { type = string }
variable "frontend_file_name" { type = string }
