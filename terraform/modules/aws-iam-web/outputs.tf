output "iam-name" {
  description = "Name of iam profile"
  value       = aws_iam_instance_profile.ec2-profile.name
}

output "iam-arn" {
  description = "iam role arn"
  value       = aws_iam_instance_profile.ec2-profile.arn
}

output "iam-id" {
  description = "iam role id"
  value       = aws_iam_instance_profile.ec2-profile.unique_id
}

