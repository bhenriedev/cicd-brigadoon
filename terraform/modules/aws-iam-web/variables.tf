variable "ec2-profile-name" {
  description = "Name of the iam profile. Must be unique."
  type        = string
}

variable "s3-bucket-id" {
    description = "ID of the bucket where we will store objects for web service"
    type        = string
}

variable "repo_credentials_arn" {
    type = string
    description = "The arn for the secret containing the credentials to login"
}