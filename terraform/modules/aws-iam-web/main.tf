resource "aws_iam_role" "ec2_assume" {
  name = "ec2_assume"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
    Name = "ec2_assume"
  }
}

resource "aws_iam_instance_profile" "ec2-profile" {
  name = var.ec2-profile-name
  role = aws_iam_role.ec2_assume.name
}

resource "aws_iam_role_policy" "s3_access" {
  name = "s3_access"
  role = aws_iam_role.ec2_assume.id

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "s3:ListBucket"
            ],
            "Resource": [
                "arn:aws:s3:::${var.s3-bucket-id}/*",
                "arn:aws:s3:::${var.s3-bucket-id}"
            ]
        },
        {
            "Effect": "Allow",
            "Action": "s3:ListAllMyBuckets",
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "fargate" {
  name = "fargate"
  role = aws_iam_role.ec2_assume.id

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
        "Action": "secretsmanager:GetSecretValue",

        "Effect": "Allow",
        
        "Resource": "${var.repo_credentials_arn}"
      }
    ]
}
EOF
}
