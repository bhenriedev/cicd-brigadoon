variable "default_vpc_id" {
  description = "Pass in ID of default VPC"
  type        = string
}