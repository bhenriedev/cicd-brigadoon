module "cicd_brigadoon_api_definitions" {
    source = "git::https://github.com/cloudposse/terraform-aws-ecs-container-definition.git"

    container_name = "cicd-brigadoon-api"
    container_image = var.container_image
    repository_credentials = tomap({
        "credentialsParameter": "${var.repo_credentials_arn}"
    })

     port_mappings = [
    {
      containerPort = 3000
      hostPort      = 3000
      protocol      = "tcp"
    }
    ]
}



resource "aws_ecs_task_definition" "this" {
    //list of containers to run
    container_definitions = jsonencode([
        module.cicd_brigadoon_api_definitions.json_map_object
    ])

    execution_role_arn =  var.execution_role_arn
    task_role_arn =  var.execution_role_arn

    family = var.cluster_name
    requires_compatibilities = [var.launch_type.type]

    cpu = var.launch_type.cpu
    memory = var.launch_type.memory
    network_mode = var.network_mode

}