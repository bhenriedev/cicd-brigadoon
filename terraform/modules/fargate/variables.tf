variable "launch_type" {
  description = "ECS launch type"
  type = object({
    type   = string
    cpu    = number
    memory = number
  })
  default = {
    type   = "FARGATE"
    cpu    = 256
    memory = 512
  }
}

variable "repo_credentials_arn" {
    type = string
    description = "The arn for the secret containing the credentials to login"
}

variable "network_mode" {
  type        = string
  description = "ECS network mode"
  default     = "awsvpc"
}

variable "cluster_name" {
  type        = string
  description = "ECS cluster name"
}

variable "vpc_id" {
    type = string
}

variable "container_image" {
    type = string
}

variable "execution_role_arn" {
    type = string
}
variable "iam_role_id" {
    type = string
}