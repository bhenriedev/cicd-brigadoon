resource "aws_ecs_cluster" "cluster" {
    name = var.cluster_name
}

resource "aws_ecs_service" "service" {
    name = "${var.cluster_name}-service"
    cluster = aws_ecs_cluster.cluster.arn  
    launch_type = var.launch_type.type
 
    deployment_maximum_percent = 200
    deployment_minimum_healthy_percent = 0
    desired_count = 1
    task_definition = "${aws_ecs_task_definition.this.family}:${aws_ecs_task_definition.this.revision}"

    network_configuration {
        assign_public_ip = true
        security_groups = data.aws_security_groups.this.ids
        subnets = data.aws_subnet_ids.this.ids
    }
}
