
data "aws_vpc" "vpc" {
  id    = var.vpc_id
}

data "aws_subnet_ids" "this" {
    vpc_id = data.aws_vpc.vpc.id
}

data "aws_security_groups" "this" {
    filter {
        name = "group-name"
        values = ["allow_tomcat"]
    }

    filter {
        name = "vpc-id"
        values = [data.aws_vpc.vpc.id]
    }
}