variable "iam_instance_profile" {
  description = "Name of the iam profile. Pull from profile creation."
  type        = string
}

variable "key_name" {
    description = "reference existing AWS key"
    type        = string
}

variable "security_groups" {
    description = "security groups to add"
    type        = list(string)
}

variable "ec2_instance_type" {
    description = "size of EC2 instance to create"
    type        = string
}

#variable "build_filename" {
#  type        = string
#  description = "file name of JAR to insert into web server"
#}

#variable "s3_bucket_id" {
#  type        = string
#  description = "ID name of the bucket from where to pull the jar"
#}

variable "instance_name" {
  description = "name of the instance to display in EC2 dashboard"
  type        = string
}

variable "image_name" {
  description = "name of the image to display in EC2 dashboard"
  type        = string
}

variable "CI_REGISTRY" {
  type = string

}
variable "CI_REGISTRY_USER" {
  type = string

}
variable "CI_REGISTRY_PASSWORD" {
  type = string

}