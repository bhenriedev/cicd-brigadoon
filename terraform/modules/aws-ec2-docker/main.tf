resource "aws_instance" "app_server" {
  ami                  = "ami-0a1069756c2859f0b"
  instance_type        = var.ec2_instance_type
  key_name             = var.key_name
  iam_instance_profile = var.iam_instance_profile
  security_groups      = var.security_groups
 
  tags = {
    Name = var.instance_name
  }


  user_data = <<EOF
#!/bin/bash
curl -sSL https://get.docker.com | bash
sudo usermod -aG docker $(whoami)
systemctl enable docker.service
systemctl start docker.service
echo ${var.CI_REGISTRY_PASSWORD} | docker login -u ${var.CI_REGISTRY_USER} ${var.CI_REGISTRY} --password-stdin
docker pull ${var.image_name}
docker run -d --name ${var.instance_name} -p 8080:3000 ${var.image_name} node server.js
EOF


}
