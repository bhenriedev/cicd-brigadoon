variable "build_filename" {
  type        = string
  description = "file name of JAR to insert into web server"
}

variable "s3_bucket_id" {
  type        = string
  description = "ID name of the bucket from where to pull the jar"
}

variable "build_filepath" {
    description = "Path to locate the file"
    type        = string
}