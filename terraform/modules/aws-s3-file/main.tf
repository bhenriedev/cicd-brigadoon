resource "aws_s3_bucket_object" "artifact" {
  bucket         = var.s3_bucket_id
  key            = var.build_filename
  source         = "${var.build_filepath}/${var.build_filename}"

  etag           = filemd5("${var.build_filepath}/${var.build_filename}")
  content_type   = "text/html"
  acl            = "public-read"
}
