import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from './../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SpeakerService {

  apiURL = environment.apiURL


  constructor(
    private http: HttpClient
    ) { }

  getHealth():Observable<{response: number}>{
    return this.http.get<{response: number}>(`${this.apiURL}/health`)
  }

  sendMessgae(message:string): Observable<{response: string}>{
    return this.http.post<{response: string}>(`${this.apiURL}/message`,{message: message});
  }
}
