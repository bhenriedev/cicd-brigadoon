import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SpeakerService } from './speaker.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Team Brigadoon';
  value = "Ask me";
  healthStatus:any;
  response:any;
  errorMessage: any;

  HealthKey = new Map<string, string>([
    ["1","Healthy"],
    ["2","Unhealthy"],
    ["3","Dead"],
  ]);


  constructor(private speaker: SpeakerService, private router: Router){

  }
  ngOnDestroy(){

  }

  checkHealth(){
    this.speaker.getHealth().subscribe((health => {
      if (health !== undefined){
        console.log(health)
        let test = health["response"].toLocaleString();
        this.healthStatus = this.HealthKey.get(test)
        console.log(test)
      } else {
        this.healthStatus = "They arent ready"
      }
    }),
    (error)=>{
      console.log(error);
      this.healthStatus = "Pending..."
      this.errorMessage = error.message
    }
    )
  }

  sendMessage(){
    this.response = this.speaker.sendMessgae(this.value).subscribe(
      (response)=>{
        this.response = response['response']
        console.log(response)
      },
      (error)=>{
        console.log(error);
        this.response =
        `We were unable to ask: ${this.value}.`
        error.message ? this.errorMessage = error.message : "";
      })
  }

  refresh(){
    this.value = "Ask me";
    this.healthStatus = "";
    this.response= "";
    this.errorMessage= ""
  }

}



