import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MATERIALS } from './materials';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MATERIALS
  ],
  exports: [MATERIALS]
})
export class StylesModule { }
