import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { StylesModule } from './styles/styles.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [
  {path: "", redirectTo: "home", pathMatch: "full"},
  { path: 'home', component: AppComponent },
]

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    StylesModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  exports: [FormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
