const request = require("supertest");

const app = require("./index");


describe("Test example", () => {
  beforeEach(() => {
    server = app.listen(3000, () => console.log('Listening on port 3000'));
  });
  let server = null;
  afterEach(async () => {
    await server.close();
});
    test("GET /", (done) => {
      request(app).get("/health")
        .expect("Content-Type", /json/)
        .expect(200)
        .expect((res) => {
         expect(res.body["response"]).toBeLessThan(4)
        })
        .end((err, res) => {
          if (err) return done(err);
          return done();
        });
    });

    test("GET /", (done) => {
      request(app).get("/health")
        .expect("Content-Type", /json/)
        .expect(200)
        .expect((res) => {
         expect(res.body["response"]).toBeGreaterThan(0)
        })
        .end((err, res) => {
          if (err) return done(err);
          return done();
        });
    });

});   
