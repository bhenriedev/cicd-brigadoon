const express = require('express');

const app = express();
const cors = require('cors');
const port = 3000;

app.use(express.json());
app.use(cors());

app.get('/health', (req, res) => {
    let number = Math.floor(Math.random() * 3) +1;
    res.status(200).json({"response": number });
});

app.post('/message', (req, res) => {
    message = req.body.message
    res.status(200).json({"response": `${message} BARK BARK`}); 
});



module.exports = app;
 